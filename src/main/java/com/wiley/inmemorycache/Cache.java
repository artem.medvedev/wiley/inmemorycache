package com.wiley.inmemorycache;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public interface Cache<K, V> {
	void clear();

	V get(K key);

	int getCapacity();

	boolean isEmpty();

	V put(K ker, V value);

	int size();
}
