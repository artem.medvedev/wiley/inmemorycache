package com.wiley.inmemorycache;

public abstract class CacheBase<K, V> implements Cache<K, V> {

	private static final int MAX_SIZE = 1 << 30;
	private static final int DEFAULT_CAPACITY = 2 * 2 * 2;

	protected final int capacity;

	@SuppressWarnings("unused")
	public CacheBase() {
		this(DEFAULT_CAPACITY);
	}

	public CacheBase(int capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException("The specified capacity is less than or equal to zero.");
		}
		if (capacity > MAX_SIZE) {
			capacity = MAX_SIZE;
		}
		this.capacity = capacity;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}
}
