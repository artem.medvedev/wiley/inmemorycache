package com.wiley.inmemorycache;

public class InMemoryCacheFactory implements CacheFactory {
	@Override
	public <K, V> Cache<K, V> create(int capacity, EvictionStrategy strategy) {
		switch (strategy) {
			default:
			case LRU: return new LRUCache<>(capacity);
			case LFU: return new LFUCache<>(capacity);
		}
	}
}
