package com.wiley.inmemorycache;

import java.util.HashMap;

public class LRUCache<K, V> extends CacheBase<K, V> {

	private static class Node<K, V> {
		final K key;
		V value;
		Node<K, V> prev;
		Node<K, V> next;

		Node(K key, V value) {
			this.key = key;
			this.value = value;
			this.prev = null;
			this.next = null;
		}

		@Override
		public String toString() {
			return "Node{" +
					"key=" + key +
					", value=" + value +
					'}';
		}
	}

	private final HashMap<K, Node<K, V>> cache;
	private Node<K, V> lru;
	private Node<K, V> mru;

	public LRUCache(int capacity) {
		super(capacity);
		this.cache = new HashMap<>();
		this.lru = null;
		this.mru = null;
	}

	private void raisePriority(Node<K, V> node) {
		if (node.key == mru.key) return;
		Node<K, V> prev = node.prev;
		Node<K, V> next = node.next;
		if (node.key == lru.key) {
			next.prev = null;
			lru = next;
		} else {
			prev.next = next;
			next.prev = prev;
		}
		node.prev = mru;
		mru.next = node;
		mru = node;
		mru.next = null;
	}

	@Override
	public V get(K key) {
		Node<K, V> node = cache.get(key);
		if (node == null) {
			return null;
		} else if (node.key == mru.key) {
			return mru.value;
		}
		raisePriority(node);
		return node.value;
	}

	@Override
	public V put(K key, V value) {
		Node<K, V> node = cache.get(key);
		if (node != null) {
			V oldValue = node.value;
			node.value = value;
			raisePriority(node);
			return oldValue;
		}
		node = new Node<>(key, value);
		if (isEmpty()) {
			lru = node;
			mru = lru;
		}
		node.prev = mru;
		mru.next = node;
		mru = node;
		if (cache.size() >= capacity) {
			cache.remove(lru.key);
			lru = lru.next;
			lru.prev = null;
		}
		cache.put(key, node);
		return null;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public int size() {
		return cache.size();
	}

	@Override
	public boolean isEmpty() {
		return cache.isEmpty();
	}

	@Override
	public void clear() {
		cache.clear();
		lru = null;
		mru = null;
	}
}
