package com.wiley.inmemorycache;

public enum EvictionStrategy {
	LRU,
	LFU
}
