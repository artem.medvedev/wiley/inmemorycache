package com.wiley.inmemorycache;

import java.util.HashMap;

public class LFUCache<K, V> extends CacheBase<K, V> {

	private static class Node<K, V> {
		private final K key;
		private V value;
		private FreqNode<K, V> parent;
		private Node<K, V> next;
		private Node<K, V> prev;

		public Node(K key, V value) {
			this.key = key;
			this.value = value;
			this.parent = null;
			this.next = null;
			this.prev = null;
		}
	}

	private static class FreqNode<K, V> {
		private int freq;
		private FreqNode<K, V> next;
		private FreqNode<K, V> prev;
		private Node<K, V> head;
		private Node<K, V> tail;

		private FreqNode(int freq) {
			this.freq = freq;
			this.head = new Node<>(null, null);
			this.tail = new Node<>(null, null);
			this.head.next = tail;
			this.tail.prev = head;
			this.next = null;
			this.prev = null;
		}

		public boolean isEmpty() {
			return head.next == tail;
		}

		public void addNode(Node<K, V> node) {
			node.next = head.next;
			head.next.prev = node;

			node.prev = head;
			head.next = node;

			node.parent = this;
		}

		public void unlinkNode(Node<K, V> node) {
			node.prev.next = node.next;
			node.next.prev = node.prev;

			node.next = null;
			node.prev = null;
			node.parent = null;
		}

		public Node<K, V> release() {
			Node<K, V> tmp = tail.prev;
			unlinkNode(tmp);
			return tmp;
		}
	}

	private final HashMap<K, Node<K, V>> cache;
	private FreqNode<K, V> head;
	private FreqNode<K, V> tail;
	private int size;

	public LFUCache(int capacity) {
		super(capacity);
		this.cache = new HashMap<>();
		this.size = 0;
		this.head = null;
		this.tail = null;
	}

	private void unlinkFreqNode(FreqNode<K, V> node) {
		node.prev.next = node.next;
		node.next.prev = node.prev;

		node.next = null;
		node.prev = null;
	}

	private void increaseFreq(Node<K, V> node) {
		FreqNode<K, V> freqNode = (node.parent == null) ? head : node.parent;
		FreqNode<K, V> nextFreqNode = freqNode.next;
		if (nextFreqNode.freq != freqNode.freq + 1) {
			nextFreqNode = new FreqNode<>(freqNode.freq + 1);

			nextFreqNode.next = freqNode.next;
			freqNode.next.prev = nextFreqNode;

			nextFreqNode.prev = freqNode;
			freqNode.next = nextFreqNode;
		}

		if (freqNode != head) {
			freqNode.unlinkNode(node);
			if (freqNode.isEmpty()) {
				unlinkFreqNode(freqNode);
			}
		}
		nextFreqNode.addNode(node);
	}

	private void release() {
		FreqNode<K, V> node = head.next;
		if (node != tail) {
			Node<K, V> tmp = node.release();
			cache.remove(tmp.key);
			if (node.isEmpty()) {
				unlinkFreqNode(node);
			}
		}
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public V get(K key) {
		Node<K, V> node = cache.get(key);
		if (node == null) {
			return null;
		}
		increaseFreq(node);
		return node.value;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public V put(K key, V value) {
		Node<K, V> node = cache.get(key);
		if (node != null) {
			node.value = value;
			increaseFreq(node);
			return value;
		}
		if (isEmpty()) {
			this.head = new FreqNode<>(0);
			this.tail = new FreqNode<>(0);
			this.head.next = tail;
			this.tail.prev = head;
		}
		if (size >= capacity) {
			release();
			size--;
		}
		node = new Node<>(key, value);
		increaseFreq(node);
		cache.put(key, node);
		size++;
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void clear() {
		cache.clear();
		size = 0;
		head = new FreqNode<>(0);
		tail = new FreqNode<>(0);
		head.next = tail;
		tail.prev = head;
	}
}
