package com.wiley.inmemorycache;

final class Utils {
	public static TestEntry<Integer, Integer> createEntry(
			int key,
			Integer value,
			TestEntryType entryType) {
		return new TestEntry<>(key, value, entryType);
	}
}
