package com.wiley.inmemorycache;

import java.util.List;

@SuppressWarnings("unused")
public interface CacheTest<K, V> {
	void cacheCapacityTest();

	void clearCacheTest();

	void getExistingValueTest();

	void getNullKeyValueTest();

	void getNotExistentValueTest();

	void putTest(List<TestEntry<K, V>> entries);
}
