package com.wiley.inmemorycache;

public class TestEntry<K, V> {
	private final K key;
	private final V value;
	private final TestEntryType type;

	public TestEntry(K key, V value, TestEntryType type) {
		this.key = key;
		this.value = value;
		this.type = type;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public TestEntryType getType() {
		return type;
	}

	@Override
	public String toString() {
		return "TestEntry{" +
				"key=" + key +
				", value=" + value +
				", type=" + type +
				'}';
	}
}
