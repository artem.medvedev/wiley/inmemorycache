package com.wiley.inmemorycache;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.wiley.inmemorycache.Utils.createEntry;
import static org.junit.jupiter.api.Assertions.*;

public class LRUCacheTest implements CacheTest<Integer, Integer> {

	protected static final int CAPACITY = 2;
	protected static final Integer DUMMY = -1;

	private final CacheFactory cacheFactory;
	private Cache<Integer, Integer> cache;

	public LRUCacheTest() {
		this.cacheFactory = new InMemoryCacheFactory();
	}

	@BeforeEach
	public void setUp() {
		cache = cacheFactory.create(CAPACITY, EvictionStrategy.LRU);
	}

	@Test
	@Override
	public void cacheCapacityTest() {
		assertEquals(0, cache.size());

		cache.put(1, DUMMY);
		cache.put(1, DUMMY);

		assertEquals(1, cache.size());

		cache.put(3, DUMMY);
		cache.put(3, DUMMY);
		cache.put(3, DUMMY);
		cache.put(4, DUMMY);
		cache.put(5, DUMMY);

		assertEquals(CAPACITY, cache.size());
	}

	@Test
	@Override
	public void getExistingValueTest() {
		cache.put(0, DUMMY);
		assertEquals(DUMMY, cache.get(0));
	}

	@Test
	@Override
	public void getNotExistentValueTest() {
		assertTrue(cache.isEmpty());
		assertNull(cache.get(0));
	}

	@Test
	@Override
	public void getNullKeyValueTest() {
		cache.put(null, DUMMY);
		assertNotNull(cache.get(null));
		cache.put(null, null);
		assertNull(cache.get(null));
	}

	@Test
	@Override
	public void clearCacheTest() {
		cache.put(1, DUMMY);
		assertFalse(cache.isEmpty());
		cache.clear();
		assertTrue(cache.isEmpty());
	}

	private static Stream<Arguments> getTestEntries() {
		return Stream.of(
				Arguments.of(
						Arrays.asList(
								createEntry(1, 1, TestEntryType.PUT),
								createEntry(2, 2, TestEntryType.PUT),
								createEntry(1, 1, TestEntryType.GET),
								createEntry(3, 3, TestEntryType.PUT),
								createEntry(2, null, TestEntryType.GET),
								createEntry(4, 4, TestEntryType.PUT),
								createEntry(1, null, TestEntryType.GET),
								createEntry(3, 3, TestEntryType.GET),
								createEntry(4, 4, TestEntryType.GET)
						)
				),
				Arguments.of(
						Arrays.asList(
								createEntry(2, 1, TestEntryType.PUT),
								createEntry(2, 2, TestEntryType.PUT),
								createEntry(2, 2, TestEntryType.GET),
								createEntry(1, 1, TestEntryType.PUT),
								createEntry(4, 1, TestEntryType.PUT),
								createEntry(2, null, TestEntryType.GET)
						)
				),
				Arguments.of(
						Arrays.asList(
								createEntry(2, 1, TestEntryType.PUT),
								createEntry(1, 1, TestEntryType.PUT),
								createEntry(2, 3, TestEntryType.PUT),
								createEntry(4, 1, TestEntryType.PUT),
								createEntry(1, null, TestEntryType.GET),
								createEntry(2, 3, TestEntryType.GET)
						)
				),
				Arguments.of(
						Arrays.asList(
								createEntry(2, null, TestEntryType.GET),
								createEntry(2, 6, TestEntryType.PUT),
								createEntry(1, null, TestEntryType.GET),
								createEntry(1, 5, TestEntryType.PUT),
								createEntry(1, 2, TestEntryType.PUT),
								createEntry(1, 2, TestEntryType.GET),
								createEntry(2, 6, TestEntryType.GET)
						)
				)
		);
	}

	@MethodSource("getTestEntries")
	@ParameterizedTest(name = "Run {index}: test entries = {0}")
	@Override
	public void putTest(List<TestEntry<Integer, Integer>> entries) {
		for (TestEntry<Integer, Integer> entry : entries) {
			switch (entry.getType()) {
				case PUT:
					cache.put(entry.getKey(), entry.getValue());
					break;
				case GET:
					assertEquals(entry.getValue(), cache.get(entry.getKey()));
					break;
			}
		}
	}
}